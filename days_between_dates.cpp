// this was a hidden function given to me. I just found some code that does the same thing
int DaysInMonth(int month, int year);
{
    if (month == 2)
	{
		if ((year%400==0) || (year%4==0 && year%100!=0)) 	
			return 29;
		else	
			return 28;
	}
	else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 ||month == 10 || month==12)	
		return 31;
	else 		
		return 30;
}  
 // Do not edit above this line. It is only shown so you can see the function signature.

// tell us how many days we are into a given date
int DaysIntoYear(int year, int month, int day)
{
    int days = day;
    for (int i = 1; i < month; i++)
    {
        days += DaysInMonth(i, year);
    }
    
    return days;
}

// give us the number of leap years between the two years so we can add those extra days. 1 day per leap year
int NumLeapYears(int year1, int year2)
{
    int num = 0;
    for (int year = year1; year < year2; year++)
    {
        if (DaysInMonth(2, year) == 29)
        {
            num++;
        }
    }
    return num;
}

int DaysBetween(int year1, int month1, int day1, int year2, int month2, int day2) {

    // 1. Obvious quick solution would be to convert each date into Julian day (running count of days) and then subtract the two dates (O(1)).
    // This solution is just about as trivial as using Date objects so I did not pursue it as it does not use the DaysInMonth() function.
    
    // 2. Brute force solution, cycle through every month and add days (O(N)) where 'N' is number of months between dates.
    //     int days_count = 0;
    //     while ((year2 * 12 + month2) - (year1 * 12 + month1) > 0)
    //     {
    //         days_count += DaysInMonth(month1, year1);
    //         if (month1 == 12)
    //         {
    //             month1 = 1;
    //             year1++;
    //         }
    //         else
    //         {
    //             month1++;   
    //         }
    //     }

    //     days_count += (day2 - day1);
    //     return days_count;
    
    // 3. Slightly optimized version of brute force. We know how many days are in a year, so skip those calculations.
    
    int days_into_year1 = DaysIntoYear(year1, month1, day1);
    int days_into_year2 = DaysIntoYear(year2, month2, day2);
    
    int days_left_in_year1 = 365 - days_into_year1;

    if (year1 == year2)
    {
        return days_into_year2 - days_into_year1;
    }
    else
    {
        return (year2 - (year1 + 1)) * 365 + days_left_in_year1 + days_into_year2 + NumLeapYears(year1, year2);
    }
}